<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>PythonShell</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.ui" line="60"/>
        <source>Run Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.ui" line="70"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.ui" line="80"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonEditorActions</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="65"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="68"/>
        <source>Create a new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="71"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="74"/>
        <source>Open an existing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="77"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="80"/>
        <source>Save the document to disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="83"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="85"/>
        <source>Save the document under a new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="88"/>
        <source>Save As &amp;Macro...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="90"/>
        <source>Save the document as a Macro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="93"/>
        <source>Save As &amp;Script...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="95"/>
        <source>Save the document as a Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="98"/>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="398"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="100"/>
        <source>Delete all scripts from disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="103"/>
        <source>Run...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="105"/>
        <source>Run the currently edited script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="108"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="111"/>
        <source>Cut the current selection&apos;s contents to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="116"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="119"/>
        <source>Undo the last edit of the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="122"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="125"/>
        <source>Redo the last undo of the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="128"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="131"/>
        <source>Copy the current selection&apos;s contents to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="136"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="139"/>
        <source>Paste the clipboard&apos;s contents into the current selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="144"/>
        <source>C&amp;lose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="147"/>
        <source>Close the script editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="150"/>
        <source>Close Current Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="153"/>
        <source>Close the current opened tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="382"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="383"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonEditorActions.cxx" line="399"/>
        <source>All scripts will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonFileIO</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="51"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="67"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="313"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="340"/>
        <source>Sorry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="52"/>
        <source>Cannot open file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="68"/>
        <source>Could not create user PythonSwap directory: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="109"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="123"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="110"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="124"/>
        <source>No Filename Given!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="131"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="212"/>
        <source>Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="132"/>
        <source>Paraview found an old automatic save file %1. Would you like to recover its content?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="213"/>
        <source>The document has been modified.
 Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="289"/>
        <source>Save File As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="289"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="320"/>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="347"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="314"/>
        <source>Could not create user Macro directory: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="320"/>
        <source>Save As Macro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="341"/>
        <source>Could not create user Script directory: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonFileIO.cxx" line="347"/>
        <source>Save As Script</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonManagerRawInputHelper</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonManager.cxx" line="107"/>
        <source>Enter Input requested by Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonManager.cxx" line="109"/>
        <source>Input: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonScriptEditor</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="84"/>
        <source>ParaView Python Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="88"/>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="94"/>
        <source>%1[*] - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="88"/>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="94"/>
        <source>Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="89"/>
        <source>File %1 saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="95"/>
        <source>File %1 opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="166"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="180"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="191"/>
        <source>&amp;Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="196"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="197"/>
        <source>Open a python script in a new tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="201"/>
        <source>Load script into current editor tab...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="203"/>
        <source>Load a python script in the current opened tab and override its content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="206"/>
        <source>Delete...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="207"/>
        <source>Delete the script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="210"/>
        <source>Run...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="212"/>
        <source>Load a python script in a new tab and run it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonScriptEditor.cxx" line="224"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonShell</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="191"/>
        <source>resetting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="211"/>
        <source>
Python %1 on %2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="443"/>
        <source>Enter Input requested by Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="444"/>
        <source>Input: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="457"/>
        <source>Run Script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="458"/>
        <source>Python Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonShell.cxx" line="458"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPythonTabWidget</name>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="179"/>
        <source>Sorry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="180"/>
        <source>Cannot open file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="232"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="299"/>
        <source>Close Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="426"/>
        <location filename="../../paraview/Qt/Python/pqPythonTabWidget.cxx" line="428"/>
        <source>New File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
