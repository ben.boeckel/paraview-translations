<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>pqClientMainWindow</name>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="17"/>
        <source>MainWindow</source>
        <translation>FenêtrePrincipale</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="53"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="58"/>
        <source>&amp;Sources</source>
        <translation>&amp;Sources</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="63"/>
        <source>Fi&amp;lters</source>
        <translation>Fi&amp;ltres</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="68"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="73"/>
        <source>&amp;View</source>
        <translation>&amp;Vue</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="78"/>
        <source>&amp;Tools</source>
        <translation>Ou&amp;tils</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="83"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="88"/>
        <source>&amp;Macros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="93"/>
        <source>&amp;Catalyst</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="98"/>
        <source>E&amp;xtractors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="118"/>
        <source>Pipeline Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="134"/>
        <source>Statistics Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="143"/>
        <source>Animation View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="155"/>
        <source>Comparative View Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="167"/>
        <source>Collaboration Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="179"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="214"/>
        <source>Memory Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="232"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="244"/>
        <source>MultiBlock Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="256"/>
        <source>Light Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="268"/>
        <source>Color Map Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="280"/>
        <source>Selection Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="301"/>
        <source>Material Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="317"/>
        <source>OSPRay support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="332"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="348"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="364"/>
        <source>Time Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="376"/>
        <source>Output Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="383"/>
        <source>OutputMessages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="398"/>
        <source>Python Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="414"/>
        <source>Python support not available!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Clients/ParaView/ParaViewMainWindow.ui" line="429"/>
        <source>Find Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
