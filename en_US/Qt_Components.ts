<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AbortAnimation</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAbortAnimation.ui" line="16"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAbortAnimation.ui" line="36"/>
        <source>Abort Saving Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAbortAnimation.ui" line="39"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAbortAnimation.ui" line="42"/>
        <source>Interrupts the saving of animation and aborts it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAbortAnimation.ui" line="45"/>
        <source>Abort Animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AnimationTimeWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="41"/>
        <source>Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="58"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAnimationTimeWidget.ui" line="98"/>
        <source>of (TDB)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalculatorWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="43"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="59"/>
        <source>sin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="75"/>
        <source>asin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="91"/>
        <source>sinh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="107"/>
        <source>dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="130"/>
        <source>(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="146"/>
        <source>cos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="162"/>
        <source>acos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="178"/>
        <source>cosh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="194"/>
        <source>mag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="214"/>
        <source>)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="230"/>
        <source>tan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="246"/>
        <source>atan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="262"/>
        <source>tanh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="278"/>
        <source>norm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="298"/>
        <source>iHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="314"/>
        <source>abs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="330"/>
        <source>ceil</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="346"/>
        <source>x^y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="362"/>
        <source>ln</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="382"/>
        <source>jHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="398"/>
        <source>sqrt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="414"/>
        <source>floor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="430"/>
        <source>exp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="446"/>
        <source>log10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="466"/>
        <source>kHat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="482"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="498"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="514"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="530"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="546"/>
        <source>Scalars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCalculatorWidget.ui" line="556"/>
        <source>Vectors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CameraKeyFrameWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="14"/>
        <source>Camera Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="36"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="41"/>
        <source>Camera Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="46"/>
        <source>Camera Focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="51"/>
        <source>Up Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="69"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table border=&quot;0&quot; style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;Define Camera Parameters&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt;&quot;&gt;Using the left pane, edit the path followed by the camera&apos;s position and focal point for the keyframe being edited.&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="92"/>
        <source>Position Control Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="109"/>
        <source>Focus Control Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="183"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="199"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="215"/>
        <source>View Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="231"/>
        <source>View Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="238"/>
        <source>Parallel Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="258"/>
        <source>Use Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraKeyFrameWidget.ui" line="265"/>
        <source>Apply to camera</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataCurrentSelectionFrame</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attribute:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="71"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="74"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="77"/>
        <source>Toggle column visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="94"/>
        <source>Invert the selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataCurrentSelectionFrame.ui" line="97"/>
        <source>Invert selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Labels&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="46"/>
        <source>&lt;p&gt;Set the array to label selected cells with&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="49"/>
        <source>Cell Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="60"/>
        <source>&lt;p&gt;Set the array to label to selected points with&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="63"/>
        <source>Point Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="76"/>
        <source>Edit selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="79"/>
        <source>Edit Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Selection Appearance&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="120"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="160"/>
        <source>&lt;p&gt;Set the color to use to show selected elements&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="123"/>
        <source>Selection Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Interactive Selection&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="163"/>
        <source>Interactive Selection Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="170"/>
        <source>Edit interactive selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFindDataSelectionDisplayFrame.ui" line="173"/>
        <source>Edit Interactive Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightsEditor</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="20"/>
        <source>Lights Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="59"/>
        <source>Turn on or off all the lights in the lighting kit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="62"/>
        <source>Light Kit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="72"/>
        <source>Reset lights to their default values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="75"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="100"/>
        <source>The two back lights, one on the left of the object as seen from the observer and one on the right, fill on the high-contrast areas behind the object. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="103"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="116"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="374"/>
        <source>Set the Key-to-Head ratio. Similar to the Key-to-Fill ratio, this ratio controls how bright the headlight light is compared to the key light: larger values correspond to a dimmer headlight light. The headlight is special kind of fill light, lighting only the parts of the object that the camera can see. As such, a headlight tends to reduce the contrast of a scene. It can be used to fill in &quot;shadows&quot; of the object missed by the key and fill lights. The headlight should always be significantly dimmer than the key light: ratios of 2 to 15 are typical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="129"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="449"/>
        <source>The &quot;Warmth&quot; of the Fill Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="132"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="225"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="520"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="530"/>
        <source>Warm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="139"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="270"/>
        <source>The Fill Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="142"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="152"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="540"/>
        <source>Azi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="149"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="355"/>
        <source>The Key Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="165"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="487"/>
        <source>The Back Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="190"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="423"/>
        <source>The Intensity of the Key Light.  0 = off and 1 = full intensity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="209"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="497"/>
        <source>Set the Key-to-Back Ratio. This ratio controls how bright the back lights are compared to the key light: larger values correspond to dimmer back lights. The back lights fill in the remaining high-contrast regions behind the object. Values between 2 and 10 are good.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="222"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="390"/>
        <source>The &quot;Warmth&quot; of the Headlight. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="238"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="517"/>
        <source>The &quot;Warmth&quot; of the Key Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="254"/>
        <source>The Fill Light is usually positioned across from or opposite from the key light (though still on the same side of the object as the camera) in order to simulate diffuse reflections from other objects in the scene.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="257"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="295"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="403"/>
        <source>The Fill Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="320"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="507"/>
        <source>The Key Light Elevation. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="339"/>
        <source>The Key Light is the main light.  It is usually positioned so that it appears like an overhead light (like the sun or a ceiling light).  It is generally positioned to shine down from about a 45 degree angle vertically and at least a little offset side to side.  The key light is usually at least about twice as bright as the total of all other lights in the scene to provide good modeling of object features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="342"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="377"/>
        <source>K:H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="406"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="490"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="510"/>
        <source>Ele</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="413"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="582"/>
        <source>Set the Key-to-Fill Ratio. This ratio controls how bright the fill light is compared to the key light: larger values correspond to a dimmer fill light. The purpose of the fill light is to light parts of the object not lit by the key light, while still maintaining contrast. This type of lighting may correspond to indirect illumination from the key light, bounced off a wall, floor, or other object. The fill light should never be brighter than the key light: a good range for the key-to-fill ratio is between 2 and 10.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="416"/>
        <source>K:F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="426"/>
        <source>Int</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="433"/>
        <source>The headlight, always located at the position of the camera, reduces the contrast between areas lit by the key and fill light. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="436"/>
        <source>Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="468"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="537"/>
        <source>The Back Light Azimuth. For simplicity, the position of lights in the LightKit can only be specified using angles: the elevation (latitude) and azimuth (longitude) of each light with respect to the camera, expressed in degrees. (Lights always shine on the camera&apos;s lookat point.) For example, a light at (elevation=0, azimuth=0) is located at the camera (a headlight). A light at (elevation=90, azimuth=0) is above the lookat point, shining down. Negative azimuth values move the lights clockwise as seen above, positive values counter-clockwise. So, a light at (elevation=45, azimuth=-20) is above and in front of the object and shining slightly from the left side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="500"/>
        <source>K:B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="527"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="563"/>
        <source>The &quot;Warmth&quot; of the Back Light. Warmth is a parameter that varies from 0 to 1, where 0 is &quot;cold&quot; (looks icy or lit by a very blue sky), 1 is &quot;warm&quot; (the red of a very red sunset, or the embers of a campfire), and 0.5 is a neutral white. The warmth scale is non-linear. Warmth values close to 0.5 are subtly &quot;warmer&quot; or &quot;cooler,&quot; much like a warmer tungsten incandescent bulb, a cooler halogen, or daylight (cooler still). Moving further away from 0.5, colors become more quickly varying towards blues and reds. With regards to aesthetics, extremes of warmth should be used sparingly. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="547"/>
        <source>If Maintain Luminance is set, the LightKit will attempt to maintain the apparent intensity of lights based on their perceptual brightnesses.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsEditor.ui" line="550"/>
        <source>Maintain Luminance</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LightsInspector</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsInspector.ui" line="14"/>
        <source>Light Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLightsInspector.ui" line="23"/>
        <source>Add Light</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultiBlockInspectorWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="14"/>
        <source>MultiBlock Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="41"/>
        <source>Extract Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="63"/>
        <source>Show/Hide legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMultiBlockInspectorWidget.ui" line="85"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Color Legend:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_color.png&quot;/&gt; - using coloring parameters from display&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_color.png&quot;/&gt; - color is inherited from a parent node or color map&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_color.png&quot;/&gt; - color explicitly specified for this node&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Opacity Legend:&lt;/span&gt;&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/no_opacity.png&quot;/&gt; - using opacity parameters from display&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/inherited_opacity.png&quot;/&gt; - opacity is inherited from a parent node&lt;br/&gt;&lt;img src=&quot;:/pqWidgets/Icons/explicit_opacity.png&quot;/&gt; - opacity explicitly specified for this node&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OrbitCreatorDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="14"/>
        <source>Create Orbit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="20"/>
        <source>Orbit Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="26"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="42"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="58"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqOrbitCreatorDialog.ui" line="92"/>
        <source>Reset Center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopoutPlaceholder</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="33"/>
        <source>Layout shown in separate window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPopoutPlaceholder.ui" line="58"/>
        <source>Click to restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyLinksConnection</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqDisplayColorWidget.cxx" line="145"/>
        <source>Change coloring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqDisplayRepresentationWidget.cxx" line="75"/>
        <source>Change representation type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyInformationWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="32"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="39"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="52"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="199"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="276"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="368"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="385"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="478"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="491"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="504"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="65"/>
        <source>0 - 100
0 - 100
0 - 100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="84"/>
        <source>Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="127"/>
        <source>Hierarchy shown here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="135"/>
        <source>Assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="178"/>
        <source>Assembly shown here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="192"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="554"/>
        <source>(n/a)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="225"/>
        <source># of Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="243"/>
        <source>Data Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="259"/>
        <source># of Vertices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="266"/>
        <source>Extents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="300"/>
        <source>Data Arrays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="327"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="343"/>
        <source>0.000	- 0.000
100.000	- 100.000
200.000	- 200.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="361"/>
        <source># of Cells</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="378"/>
        <source># of Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="398"/>
        <source># of Edges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="435"/>
        <source>Data Grouping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="451"/>
        <source>Memory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="458"/>
        <source>Bounds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="468"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="528"/>
        <source>File Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="544"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="561"/>
        <source># of TimeSteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="568"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="575"/>
        <source>Current Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyInformationWidget.ui" line="582"/>
        <source>1 (range: [0, 1])</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxySelectionWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxySelectionWidget.ui" line="31"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProxyWidgetDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="57"/>
        <source>Restore application default setting values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="67"/>
        <source>Save current settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="102"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="114"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="127"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqProxyWidgetDialog.ui" line="140"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PythonAnimationCue</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="14"/>
        <source>Edit Python Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPythonAnimationCue.ui" line="20"/>
        <source>Script:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScalarValueListPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="50"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="53"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="67"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="81"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="108"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="64"/>
        <source>Remove current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="78"/>
        <source>Add a range of values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="105"/>
        <source>Remove all entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqScalarValueListPropertyWidget.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Value Range:&lt;/span&gt;  [%1, %2]&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSearchBox.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSearchBox.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Search for properties by name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSearchBox.ui" line="29"/>
        <source>Search ... (use Esc to clear text)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSearchBox.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle advanced properties&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectReaderDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="14"/>
        <source>Open Data With...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="26"/>
        <source>A reader for FileName could not be found.  Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="43"/>
        <source>Opening the file with an incompatible reader may result in unpredictable behavior or a crash.  Please choose the correct reader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="81"/>
        <source>Set reader as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectReaderDialog.ui" line="88"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionLinkDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="6"/>
        <source>Selection Link Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="19"/>
        <source>&lt;b&gt;Link Selected Elements:&lt;/b&gt; link selection by evaluating the&lt;br/&gt;selection on the data source and select corresponding&lt;br/&gt;elements based on their indices on other linked data sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionLinkDialog.ui" line="36"/>
        <source>&lt;b&gt;Link Selection:&lt;/b&gt; link selection by sharing the actual selection&lt;br/&gt;between the data sources. The selection is then evaluated for&lt;br/&gt;each linked sources separately.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSettingsDialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSettingsDialog.ui" line="68"/>
        <source>* Restart required for some settings to take effect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>displayRepresentationWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqDisplayRepresentationWidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAboutDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="20"/>
        <source>About ParaView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="43"/>
        <source>Client Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="71"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="113"/>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="76"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="118"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="85"/>
        <source>Connection Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.kitware.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.kitware.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="155"/>
        <source>&lt;html&gt;&lt;b&gt;Version: &lt;i&gt;3.x.x&lt;/i&gt;&lt;/b&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.paraview.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:palette(link);&quot;&gt;www.paraview.org&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="186"/>
        <source>Copy to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqAboutDialog.ui" line="193"/>
        <source>Save to File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="85"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="115"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="116"/>
        <source>VTK Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="117"/>
        <source>Qt Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="119"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>vtkIdType size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="119"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="244"/>
        <source>%1bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="124"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="252"/>
        <source>Embedded Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="124"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="132"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="141"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="152"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="158"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="169"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="232"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="233"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="237"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="252"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="260"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="269"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="124"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="132"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="141"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="154"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="160"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="169"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="232"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="233"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="241"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="252"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="260"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="269"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="127"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="255"/>
        <source>Python Library Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="129"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="257"/>
        <source>Python Library Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="132"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="260"/>
        <source>Python Numpy Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="135"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="263"/>
        <source>Python Numpy Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="137"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="265"/>
        <source>Python Numpy Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="140"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="268"/>
        <source>Python Matplotlib Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="144"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="272"/>
        <source>Python Matplotlib Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="146"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="274"/>
        <source>Python Matplotlib Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="152"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="154"/>
        <source>Python Testing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="158"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="160"/>
        <source>MPI Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="164"/>
        <source>ParaView Build ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="169"/>
        <source>Disable Registry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="170"/>
        <source>Test Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="171"/>
        <source>Data Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="173"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="246"/>
        <source>SMP Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="174"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="247"/>
        <source>SMP Max Number of Threads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="183"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="285"/>
        <source>OpenGL Vendor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="184"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="286"/>
        <source>OpenGL Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="185"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="287"/>
        <source>OpenGL Renderer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="189"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="312"/>
        <source>Accelerated filters overrides available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="189"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="224"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="225"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="226"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="313"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="191"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="216"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="225"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="226"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="313"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="216"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="224"/>
        <source>Remote Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="225"/>
        <source>Separate Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="226"/>
        <source>Reverse Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="229"/>
        <source>Number of Processes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="232"/>
        <source>Disable Remote Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="233"/>
        <source>IceT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="237"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="241"/>
        <source>Tile Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="300"/>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="304"/>
        <source>Headless support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="304"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="309"/>
        <source>Not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="332"/>
        <source>Client Information:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="336"/>
        <source>
Connection Information:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="344"/>
        <source>Save to File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="345"/>
        <source>Text Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAboutDialog.cxx" line="345"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationManager</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationManager.cxx" line="235"/>
        <source>save animation geometry from a view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationManager.cxx" line="250"/>
        <source>Saving Animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationTimeWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationTimeWidget.cxx" line="233"/>
        <source>max is %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqAnimationViewWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="222"/>
        <source>unrecognized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="288"/>
        <source>Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="290"/>
        <source>Snap to Timesteps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="296"/>
        <source>Start Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="305"/>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="309"/>
        <source>Lock the start time to keep ParaView from changing it as available data times change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="314"/>
        <source>End Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="323"/>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="327"/>
        <source>Lock the end time to keep ParaView from changing it as available data times change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="340"/>
        <source>Stride</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="740"/>
        <source>Data Source to Follow:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="742"/>
        <source>Select Data Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="747"/>
        <source>Editing </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="759"/>
        <source>Animation Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="789"/>
        <source>&apos;Real time&apos; mode is deprecated and maybe removed in a near future.
 Prefer &apos;Snap to Timestep&apos; or &apos;Sequence&apos; if you need to interpolate between existing timesteps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="792"/>
        <source>Real Time mode is deprecated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="815"/>
        <source>Duration (s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="842"/>
        <source>No. Frames:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="929"/>
        <source>Toggle Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="943"/>
        <source>Remove Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="1038"/>
        <location filename="../../paraview/Qt/Components/pqAnimationViewWidget.cxx" line="1085"/>
        <source>Add Animation Track</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqArrayListModel</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqArrayListWidget.cxx" line="65"/>
        <source>New Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqArrayListWidget.cxx" line="117"/>
        <source>Array Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCameraDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="23"/>
        <source>Adjusting Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="66"/>
        <source>Standard Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="90"/>
        <source>Looking down X axis from (1, 0, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="110"/>
        <source>Looking down X axis from (-1, 0, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="130"/>
        <source>Looking down Y axis from (0, 1, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="150"/>
        <source>Looking down Y axis from (0, -1, 0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="170"/>
        <source>Looking down X axis from (0, 0, 1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="190"/>
        <source>Looking down Z axis from (0, 0, -1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="210"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="261"/>
        <source>Custom Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="301"/>
        <source>Configure custom viewpoint buttons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="304"/>
        <source>Configure ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="332"/>
        <source>Center of Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="367"/>
        <source>Reset center of rotation when camera is reset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="370"/>
        <source>Reset Center of Rotation When Camera is Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="396"/>
        <source>Rotation Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="418"/>
        <source>Define the rotation speed factor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="444"/>
        <source>Camera Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="471"/>
        <source>Focal Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="488"/>
        <source>The separation between eyes (in degrees). This is used when rendering in stereo mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="498"/>
        <source>Eye Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="511"/>
        <source> The size of the camera&apos;s lens in world coordinates. Affects ray traced depth of field rendering.
             </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="525"/>
        <source>Focal Disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="532"/>
        <source>Depth of field rendering focus distance. Only affects ray traced renderers. 0 (default) disables it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="548"/>
        <source>View Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="555"/>
        <source>View Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="562"/>
        <source>Load camera properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="565"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="581"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="588"/>
        <source>Save camera properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="591"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="598"/>
        <source>Focal Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="629"/>
        <source>Manipulate Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="655"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="672"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="760"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="806"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="662"/>
        <source>Azimuth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="679"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="715"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="799"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="813"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="686"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="696"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="722"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="741"/>
        <source>°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="767"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="789"/>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="820"/>
        <source>Roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="832"/>
        <source>Apply a manipulation to the current camera using the buttons on the left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="864"/>
        <source>Interactive View Link Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="886"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="889"/>
        <source>Hide background from linked view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="896"/>
        <source>Opacity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="903"/>
        <source>Define the view link opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCameraDialog.ui" line="910"/>
        <source>Select Interactive View Link to manage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="125"/>
        <source>Add Current Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="701"/>
        <source>Current Viewpoint %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="851"/>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="878"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="853"/>
        <source>Save Camera Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCameraDialog.cxx" line="880"/>
        <source>Load Camera Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqChangeInputDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="14"/>
        <source>Change Input Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="29"/>
        <source>Available Input Ports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqChangeInputDialog.ui" line="46"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;table style=&quot;-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;&quot;&gt;
&lt;tr&gt;
&lt;td style=&quot;border: none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Select &lt;span style=&quot; font-weight:600;&quot;&gt;INPUT0&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqChangeInputDialog.cxx" line="200"/>
        <source>Select &lt;b&gt;%1&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCollaborationPanel</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="77"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="82"/>
        <source>Participant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="110"/>
        <source>I&apos;m alone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="129"/>
        <source>Share mouse pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="159"/>
        <source>Disable further connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="171"/>
        <source>Server Connect ID </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCollaborationPanel.ui" line="202"/>
        <source>Chat room</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqColorChooserButtonWithPalettes</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="134"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqColorChooserButtonWithPalettes.cxx" line="136"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeCueWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeCueWidget.cxx" line="197"/>
        <source>Parameter Changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeCueWidget.cxx" line="281"/>
        <source>Update Parameter Values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeParameterRangeDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="14"/>
        <source>Enter Parameter Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="20"/>
        <source>Use comma-separated values to enter multiple values.
However number of values in both entires must match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="33"/>
        <source>  to  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="58"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="61"/>
        <source>Controls the direction in which the parameter is varied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="65"/>
        <source>Vary horizontally first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="70"/>
        <source>Vary vertically first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="75"/>
        <source>Only vary horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeParameterRangeDialog.ui" line="80"/>
        <source>Only vary vertically</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanel</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="20"/>
        <source>Layout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="34"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="64"/>
        <source>Automatic Parameter Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="120"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="160"/>
        <source>[Select Parameter]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="185"/>
        <source>Comma-separated values accepted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqComparativeVisPanel.ui" line="195"/>
        <source>Overlay all comparisons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeVisPanel.cxx" line="372"/>
        <source>Add parameter %1 : %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeVisPanel.cxx" line="378"/>
        <source>Add parameter Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeVisPanel.cxx" line="450"/>
        <source>Remove Parameter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqComparativeVisPanelNS</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeVisPanel.cxx" line="114"/>
        <source>unrecognized-proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqComparativeVisPanel.cxx" line="123"/>
        <source>unrecognized-property</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqConnectIdDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="20"/>
        <source>Connect ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="32"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The server requires a connection ID and the provided one does not match.&lt;br/&gt;If you don&apos;t know what is the connection ID for this server, ask its administrator.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqConnectIdDialog.ui" line="51"/>
        <source>Connect ID :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqContourControls</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="13"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="37"/>
        <source>Generate Triangles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="44"/>
        <source>Compute Scalars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="51"/>
        <source>Compute Gradients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="58"/>
        <source>Compute Normals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqContourControls.ui" line="68"/>
        <source>Contour By</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomFilterDefinitionWizard</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="13"/>
        <source>Create Custom Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="92"/>
        <source>&lt;b&gt;Choose a Name&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Enter a name for the custom filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="109"/>
        <source>&lt;b&gt;Define the Inputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the input ports.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="126"/>
        <source>&lt;b&gt;Define the Outputs&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the output ports.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="143"/>
        <source>&lt;b&gt;Define the Properties&lt;/b&gt;&lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Select and name the exposed properties.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="208"/>
        <source>The name will be used to identify the custom filter. It should be unique. The name should also indicate what the custom filter will be used for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="218"/>
        <source>Custom Filter Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="225"/>
        <source>Enter the custom filter name here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="255"/>
        <source>Select an object from the pipeline layout on the left. Then, select the input property from that object to expose. Give the input port a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="299"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="540"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="713"/>
        <source>Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="304"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="606"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="718"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="309"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="545"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="723"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="320"/>
        <source>Input Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="327"/>
        <source>Input Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="365"/>
        <source>Add Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="378"/>
        <source>Remove Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="391"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="487"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="674"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="404"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="500"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="687"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="461"/>
        <source>Add Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="474"/>
        <source>Remove Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="516"/>
        <source>Output Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="523"/>
        <source>Output Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="577"/>
        <source>Select an object from the pipeline layout on the left. Give the output port a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="613"/>
        <source>Property Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="648"/>
        <source>Add Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="661"/>
        <source>Remove Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="755"/>
        <source>Select an object from the pipeline layout on the left. Then, select the property from that object to expose. Give the property a name and add it to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="832"/>
        <source>&lt; &amp;Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="842"/>
        <source>&amp;Next &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="871"/>
        <source>&amp;Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterDefinitionWizard.ui" line="881"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomFilterManager</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="16"/>
        <source>Custom Filter Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="28"/>
        <source>Displays the list of registered custom filter definitions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="57"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="67"/>
        <source>Removes the selected custom filter definitions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="70"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="77"/>
        <source>Exports the selected custom filter definitions to a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="80"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="87"/>
        <source>Imports custom filter definitions from a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomFilterManager.ui" line="90"/>
        <source>&amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="233"/>
        <source>Open Custom Filter File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="234"/>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="253"/>
        <source>Custom Filter Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="234"/>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="253"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomFilterManager.cxx" line="252"/>
        <source>Save Custom Filter File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="20"/>
        <source>Configure Custom Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="50"/>
        <source>Viewpoint Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="67"/>
        <source>Assign</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="84"/>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="128"/>
        <source>Add new custom viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="142"/>
        <source>Clear All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="149"/>
        <source>Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqCustomViewpointButtonDialog.ui" line="156"/>
        <source>Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="320"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="323"/>
        <source>Load Custom Viewpoints Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="439"/>
        <source>Save Custom Viewpoints Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="509"/>
        <source>Current Viewpoint %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonDialogUI</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="83"/>
        <source>This text will be set to the buttons tool tip.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="88"/>
        <source>Current Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqCustomViewpointButtonFileInfo</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqCustomViewpointButtonDialog.cxx" line="203"/>
        <source>Unnamed Viewpoint</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDataInformationWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqDataInformationWidget.cxx" line="180"/>
        <source>Column Titles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplayColorWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqDisplayColorWidget.cxx" line="482"/>
        <source>Solid Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqDisplayColorWidget.cxx" line="587"/>
        <source>Change color component</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDisplayRepresentationWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqDisplayRepresentationWidget.cxx" line="189"/>
        <source>Representation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqDoubleVectorPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="313"/>
        <source>Reset using current data values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqDoubleVectorPropertyWidget.cxx" line="345"/>
        <source>Reset to active data bounds</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionChooserButton</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionChooserButton.cxx" line="48"/>
        <source>Choose Expression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionsManagerDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="14"/>
        <source>Choose Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="81"/>
        <source>Use current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="84"/>
        <source>Use expression in current filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="91"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="94"/>
        <source>Add a new expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="104"/>
        <source>Remove selected expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="107"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="117"/>
        <source>Remove all expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="120"/>
        <source>Remove All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="134"/>
        <source>Import expressions from a json file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="137"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="144"/>
        <source>Export expressions to a json file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="147"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="167"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="170"/>
        <source>Close without saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="177"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="180"/>
        <source>Save to settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="187"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqExpressionsDialog.ui" line="190"/>
        <source>Save and close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="405"/>
        <source>Remove all expressions ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="484"/>
        <source>Export Expressions(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="485"/>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="526"/>
        <source>ParaView Expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="485"/>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="526"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsDialog.cxx" line="525"/>
        <source>Import Expressions(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqExpressionsWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsWidget.cxx" line="85"/>
        <source>Save expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsWidget.cxx" line="90"/>
        <source>Open Expressions Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsWidget.cxx" line="99"/>
        <source>Saved! </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqExpressionsWidget.cxx" line="99"/>
        <source>Already Exists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFavoritesDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="14"/>
        <source>Favorites Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="23"/>
        <source>&lt;&lt;&lt; Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="36"/>
        <source>Available Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="79"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="153"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="113"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="161"/>
        <source>Add Category ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqFavoritesDialog.ui" line="197"/>
        <source>Add &gt;&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFileChooserWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqFileChooserWidget.cxx" line="174"/>
        <source>Open Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFileChooserWidget.cxx" line="178"/>
        <source>Save File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFileChooserWidget.cxx" line="182"/>
        <source>Open File:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqFindDataSelectionDisplayFrame</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="168"/>
        <source>not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="186"/>
        <source>%1 (partial)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="249"/>
        <source>Change labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="424"/>
        <source>Interactive selection label properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="426"/>
        <source>Interactive Selection Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="462"/>
        <source>Change selection display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqFindDataSelectionDisplayFrame.cxx" line="464"/>
        <source>Selection Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqInputSelectorWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqInputSelectorWidget.cxx" line="156"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqInternals</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="124"/>
        <source>Resize Frame</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqItemViewSearchWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="84"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="136"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="158"/>
        <source>Next (ALT+N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="187"/>
        <source>Match Case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="206"/>
        <source>Close (Esc)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqItemViewSearchWidget.ui" line="235"/>
        <source>Previous (ALT+P)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditor</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="13"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="37"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="54"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="57"/>
        <source>Add a new keyframe before selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="64"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="67"/>
        <source>Delete selected keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="74"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="77"/>
        <source>Delete all keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="84"/>
        <source>Create Orbit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="87"/>
        <source>Create an orbit path for selected keyframe, starting with current camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="94"/>
        <source>Apply to Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="97"/>
        <source>Apply selected keyframe configuration to the current view camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="104"/>
        <source>Use current Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="107"/>
        <source>Use current camera for selected keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="114"/>
        <source>Spline Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameEditor.ui" line="117"/>
        <source>Use spline interpolation for this cue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="488"/>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="494"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="489"/>
        <source>Camera Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="495"/>
        <source>Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="495"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="565"/>
        <source>Edit Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameEditorDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameEditor.cxx" line="77"/>
        <source>Key Frame Interpolation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqKeyFrameTypeWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="21"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="36"/>
        <source>Interpolation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="43"/>
        <location filename="../../paraview/Qt/Components/pqKeyFrameTypeWidget.cxx" line="58"/>
        <source>Exponential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="55"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="159"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="62"/>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="69"/>
        <source>Start Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="76"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="83"/>
        <source>End Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="90"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="166"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="116"/>
        <location filename="../../paraview/Qt/Components/pqKeyFrameTypeWidget.cxx" line="60"/>
        <source>Sinusoid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="128"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="135"/>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqKeyFrameTypeWidget.ui" line="142"/>
        <source>Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameTypeWidget.cxx" line="56"/>
        <source>Ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqKeyFrameTypeWidget.cxx" line="61"/>
        <source>Boolean</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightsEditor</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsEditor.cxx" line="123"/>
        <source>Restore Default Lights</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLightsInspector</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="181"/>
        <source>Light %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="192"/>
        <source>Move to Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="196"/>
        <source>Match this light&apos;s position and focal point to the camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="202"/>
        <source>Reset Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="206"/>
        <source>Reset this light parameters to default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="211"/>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="366"/>
        <source>Remove Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="215"/>
        <source>Remove this light.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="283"/>
        <source>Add Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="363"/>
        <source>remove light added to the view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="402"/>
        <source>reset a light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLightsInspector.cxx" line="426"/>
        <source>update a light</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinksEditor</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="103"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="113"/>
        <source>Mode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="121"/>
        <source>Object Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="126"/>
        <source>Property Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="131"/>
        <source>Selection Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="154"/>
        <source>Interactive View Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="164"/>
        <source>&lt;span&gt;When enabled, selection is linked by evaluating the selection on the data source and select corresponding elements based on their indices on other linked data sources, instead of sharing the actual selection between the data sources.&lt;/span&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksEditor.ui" line="167"/>
        <source>Link Selected Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksEditor.cxx" line="259"/>
        <source>Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksEditor.cxx" line="261"/>
        <source>Representations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksEditor.cxx" line="263"/>
        <source>Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksEditor.cxx" line="265"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksEditor.cxx" line="309"/>
        <source>Source &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;View &lt;b&gt;%2&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLinksManager</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksManager.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksManager.ui" line="33"/>
        <source>Add...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksManager.ui" line="40"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLinksManager.ui" line="47"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksManager.cxx" line="81"/>
        <source>Add Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLinksManager.cxx" line="117"/>
        <source>Edit Link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLiveInsituManager</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="233"/>
        <source>Catalyst Server Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="234"/>
        <source>Enter the port number to accept connections
from Catalyst on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="248"/>
        <source>Ready for Catalyst connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="249"/>
        <source>Accepting connections from Catalyst Co-Processor
for live-coprocessing on port %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="288"/>
        <source>Catalyst Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLiveInsituManager.cxx" line="289"/>
        <source>Connection to Catalyst Co-Processor has been terminated involuntarily. This implies either a communication error, or that the Catalyst co-processor has terminated. The Catalyst session will now be cleaned up. You can start a new one if you want to monitor for additional Catalyst connection requests.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLockViewSizeCustomDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="14"/>
        <source>Lock View to Custom Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="20"/>
        <source>Select Maximum Resolution for Each View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLockViewSizeCustomDialog.ui" line="39"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLockViewSizeCustomDialog.cxx" line="53"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqLogViewerDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="14"/>
        <source>Log Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="51"/>
        <source>New Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="79"/>
        <source>Application process to log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="92"/>
        <source>Process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="99"/>
        <source>Rank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="112"/>
        <source>Rank of process to log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="127"/>
        <source>Add Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="149"/>
        <source>Global Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="175"/>
        <source>Global filter that applies to all logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="181"/>
        <source>Set filter on all logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="203"/>
        <source>Verbosity Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="223"/>
        <source>Client Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="233"/>
        <source>Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="243"/>
        <source>Data Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="253"/>
        <source>Render Server Verbosity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="284"/>
        <source>Categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="304"/>
        <source>Always Log Messages About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="326"/>
        <source>Data Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="333"/>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="340"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="347"/>
        <source>Pipeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="354"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="361"/>
        <source>Filter Execution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="393"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqLogViewerDialog.ui" line="400"/>
        <source>Clear Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="138"/>
        <source>Data Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="139"/>
        <source>Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="152"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="374"/>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="375"/>
        <source>Close log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="421"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="422"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="423"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="424"/>
        <source>INFO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqLogViewerDialog.cxx" line="425"/>
        <source>TRACE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanel</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="538"/>
        <source>System Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1485"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1494"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1486"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1495"/>
        <source>No process selected. Select a specific process first by clicking on its entry in the tree view widget.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1599"/>
        <source>Client System Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1599"/>
        <source>Server System Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1600"/>
        <source>Host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1601"/>
        <source>OS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1602"/>
        <source>CPU:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1603"/>
        <source>Memory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1636"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1676"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1689"/>
        <source>properties...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1647"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1682"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1703"/>
        <source>remote command...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1654"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1695"/>
        <source>show only nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1655"/>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1696"/>
        <source>show all ranks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMemoryInspectorPanel.cxx" line="1717"/>
        <source>stack trace signal handler</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMemoryInspectorPanelForm</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqMemoryInspectorPanelForm.ui" line="76"/>
        <source>Auto-update</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqMultiViewWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="759"/>
        <source>Split View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="783"/>
        <source>Close View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="813"/>
        <source>Destroy all views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="878"/>
        <source>Swap Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="959"/>
        <source>Exit preview mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqMultiViewWidget.cxx" line="960"/>
        <source>Enter preview mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineBrowserWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineBrowserWidget.cxx" line="281"/>
        <source>Show %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineBrowserWidget.cxx" line="282"/>
        <source>Hide %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineBrowserWidget.cxx" line="286"/>
        <source>Show Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineBrowserWidget.cxx" line="286"/>
        <source>Hide Selected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineModel</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineModel.cxx" line="902"/>
        <source>Rename %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPipelineTimeKeyFrameEditor</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="41"/>
        <source>Variable Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="71"/>
        <source>Constant Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPipelineTimeKeyFrameEditor.ui" line="90"/>
        <source>Animation Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPipelineTimeKeyFrameEditor.cxx" line="132"/>
        <source>Edit Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPluginDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="14"/>
        <source>Plugin Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="20"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="39"/>
        <source>Remote Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="71"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="141"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="79"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="159"/>
        <source>Load New ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="89"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="152"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="99"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="169"/>
        <source>Load Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPluginDialog.ui" line="109"/>
        <source>Local Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="93"/>
        <source>Local plugins are automatically searched for in %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="99"/>
        <source>Remote plugins are automatically searched for in %1.
Local plugins are automatically searched for in %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="145"/>
        <source>Qt binary resource files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="146"/>
        <source>XML plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="149"/>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="154"/>
        <source>Binary plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="182"/>
        <source>Supported plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="261"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="261"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="328"/>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="542"/>
        <source>Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="336"/>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="542"/>
        <source>Not Loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="344"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="353"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="363"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="373"/>
        <source>Required Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="383"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="395"/>
        <source>Auto Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="472"/>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="484"/>
        <source>Remove plugin?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="473"/>
        <location filename="../../paraview/Qt/Components/pqPluginDialog.cxx" line="485"/>
        <source>Are you sure you want to remove this plugin?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPresetDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="14"/>
        <source>Choose Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="54"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Tip: &amp;lt;click&amp;gt; to select, &amp;lt;double-click&amp;gt; to apply a preset.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="63"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Options to load:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load colors from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="80"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="93"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load opacities from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="96"/>
        <source>Opacities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uncheck to not load annotations from the selected preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="112"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="125"/>
        <source>Check to use following regexp when loading annotations from the selected preset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="128"/>
        <source>Use Regular Expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This regexp will be applied to seriesName and preset value. If any, matching groups are used, otherwise whole matching. &lt;/p&gt;
           E.g. series name is `y1 ˓custom˒`, preset have `y1 ˓generic˒` and `z1 ˓custom˒`. Then with the regexp `(.*) ˓.*˒` the `y1 ˓custom˒` series will match with `y1 ˓generic˒`, whereas with `.* (˓custom˒)` it will match with `z1 ˓custom˒`.
           &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
       </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="147"/>
        <source>(.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to use data range specified in the preset.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="160"/>
        <source>Use preset range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="180"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Actions on selected:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="194"/>
        <source>Show current preset
in default mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="218"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="225"/>
        <source>Import a preset from a json file, imported preset will appear in italics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="228"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="238"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="248"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPresetDialog.ui" line="255"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="885"/>
        <source>Import Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="886"/>
        <source>Supported Presets/Color Map Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="887"/>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="925"/>
        <source>ParaView Color/Opacity Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="887"/>
        <source>Legacy Color Maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="888"/>
        <source>VisIt Color Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="888"/>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="925"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPresetDialog.cxx" line="924"/>
        <source>Export Preset(s)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPropertiesPanel</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="526"/>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="541"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="595"/>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="602"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="647"/>
        <source>View (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="656"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="831"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertiesPanel.cxx" line="893"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqPropertyWidget.cxx" line="156"/>
        <source>Property Changed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyEditorPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyEditorPropertyWidget.cxx" line="54"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyInformationWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="419"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="420"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="520"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="548"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="557"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="581"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="582"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="583"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="611"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="612"/>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="613"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="522"/>
        <source>Data Statistics (# of datasets: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="523"/>
        <source>Data Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="563"/>
        <source>%1 to %2 (delta: %3)
%4 to %5 (delta: %6)
%7 to %8 (delta: %9)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="592"/>
        <source>%1 to %2 (dimension: %3)
%4 to %5 (dimension: %6)
%7 to %8 (dimension: %9)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="629"/>
        <source>%1 (range: [%2, %3])</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyInformationWidget.cxx" line="664"/>
        <source>Show hierarchy / assembly as text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqProxyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyWidget.cxx" line="1477"/>
        <source>Use as Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqProxyWidget.cxx" line="1491"/>
        <source>Reset to Application Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRecentFilesMenu</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqRecentFilesMenu.cxx" line="233"/>
        <source>Disconnect from current server?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqRecentFilesMenu.cxx" line="234"/>
        <source>The file you opened requires connecting to a new server.
The current connection will be closed.

Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqRemoteCommandDialog.cxx" line="538"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqRemoteCommandDialog.cxx" line="540"/>
        <source>Find file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandDialogForm</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="26"/>
        <source>Remote Command Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="32"/>
        <source>Command Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="93"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="124"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="152"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="170"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="179"/>
        <source>FE_URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="189"/>
        <source>ssh url to cluster login (eg user@login.com)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="196"/>
        <source>SSH_EXEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="205"/>
        <source>path to ssh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="244"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="301"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="253"/>
        <source>TERM_EXEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="262"/>
        <source>path to terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="310"/>
        <source>TERM_OPTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="317"/>
        <source>terminal options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="327"/>
        <source>PV_HOST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="340"/>
        <source>Command Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="383"/>
        <source>execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandDialogForm.ui" line="393"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRemoteCommandTemplateDialogForm</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="14"/>
        <source>Remote Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="20"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRemoteCommandTemplateDialogForm.ui" line="34"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqRescaleRangeDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="14"/>
        <source>Set Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="20"/>
        <source>Enter the range for the opacity map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="27"/>
        <source>Enter the new range minimum for the opacity map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="33"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="129"/>
        <source>minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="40"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="119"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="68"/>
        <source>Rescale and lock the color map to avoid automatic rescaling.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="71"/>
        <source>Rescale and disable automatic rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="81"/>
        <source>Rescale and leave automatic rescaling mode unchanged.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="84"/>
        <source>Rescale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="91"/>
        <source>Close without rescaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="94"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enter the range for the color map&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="126"/>
        <source>Enter the new range minimum for the color map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="149"/>
        <source>Enter the new range maximum for the color map here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="152"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="165"/>
        <source>maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqRescaleRangeDialog.ui" line="159"/>
        <source>Enter the new range maximum for the opacity map here.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqScalarValueListPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqScalarValueListPropertyWidget.cxx" line="377"/>
        <location filename="../../paraview/Qt/Components/pqScalarValueListPropertyWidget.cxx" line="378"/>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectReaderDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectReaderDialog.cxx" line="93"/>
        <source>A reader for &quot;%1&quot; could not be found. Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectReaderDialog.cxx" line="108"/>
        <source>More than one reader for &quot;%1&quot; found. Please choose one:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionInputWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="47"/>
        <source>Copy Active Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSelectionInputWidget.ui" line="69"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="103"/>
        <source>Copied Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="98"/>
        <source>No selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="114"/>
        <source>Number of Selections: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="115"/>
        <source>Selection Expression: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="117"/>
        <source>Invert Selection: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="118"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="119"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="136"/>
        <source>Elements: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="142"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="145"/>
        <source>Frustum Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="145"/>
        <source>Values:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="164"/>
        <source>Values Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="164"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="339"/>
        <source>Array Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="166"/>
        <source>Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="184"/>
        <source>Pedigree ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="189"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="207"/>
        <source>Pedigree Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="189"/>
        <source>String ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="207"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="224"/>
        <source>Global ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="224"/>
        <source>Global ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="236"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="255"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="274"/>
        <source>ID Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="236"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="256"/>
        <source>Process ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="237"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="256"/>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="275"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="255"/>
        <source>Composite ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="275"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="275"/>
        <source>Dataset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="294"/>
        <source>Location-based Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="295"/>
        <source>Probe Locations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="313"/>
        <source>Block Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="313"/>
        <source>Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="326"/>
        <source>Block Selectors Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="326"/>
        <source>Assembly Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="328"/>
        <source>Block Selectors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="339"/>
        <source>Threshold Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="341"/>
        <source>Thresholds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="359"/>
        <source>Query Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="359"/>
        <source>Query: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="371"/>
        <source>Number of Selections: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionInputWidget.cxx" line="435"/>
        <source>Copied Selection (Active Selection Changed)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSelectionManager</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionManager.cxx" line="187"/>
        <source>clear all selections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSelectionManager.cxx" line="205"/>
        <source>clear selection for source</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerConnectDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="14"/>
        <source>Choose Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="38"/>
        <source>Add Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="48"/>
        <source>Edit Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="58"/>
        <source>Delete Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="65"/>
        <source>Load Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="72"/>
        <source>Save Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="79"/>
        <source>Fetch Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="103"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="106"/>
        <source>Delete all user server configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="109"/>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="689"/>
        <source>Delete All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="133"/>
        <source>Timeout (s) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="140"/>
        <source>Specify the timeout in seconds to use when waiting for server connection. Set it to -1 to wait indefinitely. This value will be saved in your settings for this server once you click connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="159"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="169"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="212"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="217"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="664"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="229"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="245"/>
        <source>Server Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="256"/>
        <source>Client / Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="261"/>
        <source>Client / Server (reverse connection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="266"/>
        <source>Client / Data Server / Render Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="271"/>
        <source>Client / Data Server / Render Server (reverse connection)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="279"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="286"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="320"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="354"/>
        <source>localhost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="293"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="313"/>
        <source>Data Server host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="327"/>
        <source>Data Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="347"/>
        <source>Render Server host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="361"/>
        <source>Render Server port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="412"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="419"/>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="712"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="432"/>
        <source>Configure server foobar (cs://foobar)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="439"/>
        <source>Please configure the startup procedure to be used when connecting to this server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="466"/>
        <source>Startup Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="477"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="482"/>
        <source>Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="524"/>
        <source>Manual Startup - no attempt will be made to start the server.  You must start the server manually before trying to connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="547"/>
        <source>Execute an external command to start the server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="565"/>
        <source>After executing the startup command, wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="572"/>
        <source> seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="588"/>
        <source>before connecting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="659"/>
        <source>Configuration Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="669"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="679"/>
        <source>Edit Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="702"/>
        <source>Import Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerConnectDialog.ui" line="728"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Helvetica&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Enter list of URLs to obtain the servers configurations from, using the syntax:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;&amp;lt;type&amp;gt; &amp;lt;url&amp;gt; &amp;lt;userfriendly name&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="73"/>
        <source>Enter list of URLs to obtain server configurations from.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="75"/>
        <source>Syntax:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="76"/>
        <source>userfriendly-name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="77"/>
        <source>Official Kitware Server Configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="258"/>
        <source>Edit Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="262"/>
        <source>Edit Server Launch Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="266"/>
        <source>Fetch Server Configurations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="270"/>
        <source>Edit Server Configuration Sources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="275"/>
        <source>Choose Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="330"/>
        <source>My Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="673"/>
        <source>Delete Server Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="674"/>
        <source>Are you sure you want to delete &quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="689"/>
        <source>All servers will be deleted. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="703"/>
        <source>Save Server Configuration File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="721"/>
        <source>Load Server Configuration File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="858"/>
        <source>Fetching configurations ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="879"/>
        <source>Authenticate Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="881"/>
        <source>%1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="884"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="888"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerConnectDialog.cxx" line="889"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerLauncher</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="249"/>
        <source>Connection Options for &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="638"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting for server to connect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="641"/>
        <source>Waiting for Server Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="645"/>
        <source>Establishing connection to &apos;%1&apos;
Waiting %2 seconds for connection to server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="649"/>
        <source>Waiting for Connection to Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="665"/>
        <source>Connection Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="666"/>
        <source>Unable to connect sucessfully. Try again for %1 seconds ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqServerLauncher.cxx" line="768"/>
        <source>Launching server &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqServerLauncherDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="14"/>
        <source>Starting Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="26"/>
        <source>Please wait while server cs://foobar starts ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqServerLauncherDialog.ui" line="57"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSetBreakpointDialog</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="14"/>
        <source>Set Breakpoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="20"/>
        <source>Breakpoint:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="30"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqSetBreakpointDialog.ui" line="50"/>
        <source>Time Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSetBreakpointDialog.cxx" line="92"/>
        <source>Breakpoint time is invalid or it is smaller than current time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqShaderReplacementsSelectorPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="89"/>
        <source>List of previously loaded Json files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="95"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="96"/>
        <source>Load a Json preset file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="102"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="103"/>
        <source>Delete the selected preset from the preset list or clear the current shader replacement string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="166"/>
        <source>Shader Replacements Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="196"/>
        <source>Shader replacements files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="196"/>
        <source>All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqShaderReplacementsSelectorPropertyWidget.cxx" line="197"/>
        <source>Open ShaderReplacements:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSignalAdaptorKeyFrameType</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="127"/>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqSignalAdaptorKeyFrameType.cxx" line="131"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqSpreadSheetColumnsVisibility</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqSpreadSheetColumnsVisibility.cxx" line="130"/>
        <source>All Columns</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqStringVectorPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqStringVectorPropertyWidget.cxx" line="205"/>
        <source>Select %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTabbedMultiViewWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="145"/>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="146"/>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="863"/>
        <source>Close layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="641"/>
        <source>Remove View Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="671"/>
        <source>Add View Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="715"/>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="872"/>
        <source>Close Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="862"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="864"/>
        <source>Rearrange Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="865"/>
        <source>Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="866"/>
        <source>Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="867"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="881"/>
        <source>Rename Layout...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTabbedMultiViewWidget.cxx" line="881"/>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTextureComboBox</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureComboBox.cxx" line="127"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureComboBox.cxx" line="130"/>
        <source>Load ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureComboBox.cxx" line="165"/>
        <source>Open Texture:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTextureSelectorPropertyWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="60"/>
        <source>Select/Load texture to apply.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="114"/>
        <source>Texture Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="145"/>
        <source>No tcoords present in the data. Cannot apply texture.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTextureSelectorPropertyWidget.cxx" line="151"/>
        <source>No tangents present in the data. Cannot apply texture.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTimerLogDisplay</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="13"/>
        <source>Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="33"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="40"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="68"/>
        <source>Time Threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="101"/>
        <source>Buffer Length:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="113"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqTimerLogDisplay.ui" line="123"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqTimerLogDisplay.cxx" line="292"/>
        <source>Save Timer Log</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunction2DWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqTransferFunction2DWidget.cxx" line="242"/>
        <source>Double click to add a box. Grab and drag to move the box.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqTransferFunctionWidget</name>
    <message>
        <location filename="../../paraview/Qt/Components/pqTransferFunctionWidget.cxx" line="669"/>
        <source>Drag a handle to change the range. Double click it to set custom range. Return/Enter to edit color.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pqViewFrame</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqViewFrame.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqViewFrame.cxx" line="91"/>
        <source>Split Vertical Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqViewFrame.cxx" line="94"/>
        <source>Split Horizontal Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqViewFrame.cxx" line="98"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqViewFrame.cxx" line="102"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/pqViewFrame.cxx" line="106"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>propertiesPanel</name>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="14"/>
        <source>Properties Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="25"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="39"/>
        <source>Resets any changed properties to their values from the last time &apos;Apply&apos; was clicked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="42"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="53"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="71"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="130"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="149"/>
        <source>Copy properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="172"/>
        <source>Paste copied properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="192"/>
        <source>Restore application default setting values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="212"/>
        <source>Save current settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="250"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="269"/>
        <source>Copy display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="292"/>
        <source>Paste copied display properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="312"/>
        <source>Restore application default setting values for display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="332"/>
        <source>Save current display settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="370"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="389"/>
        <source>Copy view properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="412"/>
        <source>Paste view properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="432"/>
        <source>Restore application default setting values for view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../paraview/Qt/Components/Resources/UI/pqPropertiesPanel.ui" line="452"/>
        <source>Save current view settings values as default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
