cmake_minimum_required(VERSION 3.0)
project(ParaViewTranslations NONE)

find_package(Qt5 REQUIRED QUIET COMPONENTS Core LinguistTools)

set(INSTALL_SUBDIR "share/translations" CACHE STRING "Destination of the 
  result files within the INSTALL_PREFIX directory")

set(ts_files
  Clients_ParaView.ts
  Qt_ApplicationComponents.ts
  Qt_Core.ts
  Qt_Widgets.ts
  Clients_ParaView-XMLs.ts
  Qt_Components.ts
  Qt_Python.ts
  ServerManager-XMLs.ts
  )

set(languages
  en_US
  fr_FR
  )

message(STATUS "Available languages: ${languages}")

add_custom_target("files_update")

foreach (output_language IN LISTS languages)
  set(ts_language_files ${ts_files})
  list(TRANSFORM ts_language_files PREPEND "${CMAKE_SOURCE_DIR}/${output_language}/")
  # Create qm generation targets
  set(destination_qm "${CMAKE_BINARY_DIR}/paraview_${output_language}.qm")
  add_custom_command(
    OUTPUT "${destination_qm}"
    COMMAND "$<TARGET_FILE:Qt5::lconvert>" ${ts_language_files} -o "${destination_qm}"
    DEPENDS ${ts_language_files})
  add_custom_target("${output_language}" ALL DEPENDS "${destination_qm}")
  install(
    FILES "${destination_qm}"
    DESTINATION "${INSTALL_SUBDIR}"
  )
  # Create ts files update target
  if ("${output_language}" STREQUAL "en_US")
    # Skip en_US as they are the template files
    continue()
  endif()
  add_custom_target("${output_language}_update")
  foreach (ts_file IN LISTS ts_files)
    set(absolute_ts_file "${CMAKE_SOURCE_DIR}/${output_language}/${ts_file}")
    set(absolute_en_file "${CMAKE_SOURCE_DIR}/en_US/${ts_file}")
    message(STATUS "${absolute_ts_file}")
    message(STATUS "${absolute_en_file}")
    add_custom_target("${output_language}_${ts_file}_update")
    add_custom_command(
      TARGET "${output_language}_${ts_file}_update"
      COMMAND "$<TARGET_FILE:Qt5::lupdate>" "${absolute_en_file}" -ts "${absolute_ts_file}"
      )
    add_dependencies("${output_language}_update" "${output_language}_${ts_file}_update")
  endforeach ()
  add_dependencies("files_update" "${output_language}_update")
endforeach ()
